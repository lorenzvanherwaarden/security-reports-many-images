{
  "version": "15.0.6",
  "vulnerabilities": [
    {
      "id": "4313e08bd9bcbe95b767a8c92c140f564c37fd3b9a2b2b43874cbb9d59ba4ead",
      "name": "ApexBadCrypto",
      "description": "The rule makes sure you are using randomly generated IVs and keys for Crypto calls. Hard-wiring these values greatly compromises the security of encrypted data.",
      "severity": "Medium",
      "solution": "Apex classes should use random IV/key",
      "location": {
        "file": "src/classes/AppWorker.cls",
        "start_line": 12
      },
      "identifiers": [
        {
          "type": "pmd_apex_rule_id",
          "name": "PMD Apex Rule ID Security-ApexBadCrypto",
          "value": "Security-ApexBadCrypto",
          "url": "https://pmd.github.io/pmd-6.44.0/pmd_rules_apex_security.html#apexbadcrypto"
        }
      ]
    },
    {
      "id": "4d5fe9aa462d70dc8b4d1c0cc295c1e38cd018a04c07a19d07cde9fd21fd32c5",
      "name": "ApexCRUDViolation",
      "description": "The rule validates you are checking for access permissions before a SOQL/SOSL/DML operation. Since Apex runs in system mode not having proper permissions checks results in escalation of privilege and may produce runtime errors. This check forces you to handle such scenarios.",
      "severity": "Medium",
      "solution": "Validate CRUD permission before SOQL/DML operation",
      "location": {
        "file": "src/classes/AppWorker.cls",
        "start_line": 18
      },
      "identifiers": [
        {
          "type": "pmd_apex_rule_id",
          "name": "PMD Apex Rule ID Security-ApexCRUDViolation",
          "value": "Security-ApexCRUDViolation",
          "url": "https://pmd.github.io/pmd-6.44.0/pmd_rules_apex_security.html#apexcrudviolation"
        }
      ]
    },
    {
      "id": "098e95f9ef44d32bb0320bb8559649144ac778dc20ef7c6bcb58ca137c906c58",
      "name": "ApexDangerousMethods",
      "description": "Checks against calling dangerous methods. For the time being, it reports: Against FinancialForce’s 'Configuration.disableTriggerCRUDSecurity()'. Disabling CRUD security opens the door to several attacks and requires manual validation, which is unreliable. Calling 'System.debug' passing sensitive data as parameter, which could lead to exposure of private data.",
      "severity": "Medium",
      "solution": "Calling potentially dangerous method",
      "location": {
        "file": "src/classes/AppWorker.cls",
        "start_line": 34
      },
      "identifiers": [
        {
          "type": "pmd_apex_rule_id",
          "name": "PMD Apex Rule ID Security-ApexDangerousMethods",
          "value": "Security-ApexDangerousMethods",
          "url": "https://pmd.github.io/pmd-6.44.0/pmd_rules_apex_security.html#apexdangerousmethods"
        }
      ]
    },
    {
      "id": "6f78507a9f1d01d7c6652279d7177527487e116e0f2fa45685bbfccf8d2b4db3",
      "name": "ApexInsecureEndpoint",
      "description": "Checks against accessing endpoints under plain http. You should always use https for security.",
      "severity": "Medium",
      "solution": "Apex callouts should use encrypted communication channels",
      "location": {
        "file": "src/classes/AppWorker.cls",
        "start_line": 43
      },
      "identifiers": [
        {
          "type": "pmd_apex_rule_id",
          "name": "PMD Apex Rule ID Security-ApexInsecureEndpoint",
          "value": "Security-ApexInsecureEndpoint",
          "url": "https://pmd.github.io/pmd-6.44.0/pmd_rules_apex_security.html#apexinsecureendpoint"
        }
      ]
    },
    {
      "id": "5d5b134a60d5d3d97985bb4aa2dc8530c415e1fb08a6b2e076dea3b70798ac34",
      "name": "ApexOpenRedirect",
      "description": "Checks against redirects to user-controlled locations. This prevents attackers from redirecting users to phishing sites.",
      "severity": "Medium",
      "solution": "Apex classes should safely redirect to a known location",
      "location": {
        "file": "src/classes/AppWorker.cls",
        "start_line": 52
      },
      "identifiers": [
        {
          "type": "pmd_apex_rule_id",
          "name": "PMD Apex Rule ID Security-ApexOpenRedirect",
          "value": "Security-ApexOpenRedirect",
          "url": "https://pmd.github.io/pmd-6.44.0/pmd_rules_apex_security.html#apexopenredirect"
        }
      ]
    },
    {
      "id": "10e76f285c8ca6c4cab5ff68c4daf5fe4512d8f57859b5f94455ea8eb63b4cc0",
      "name": "ApexSOQLInjection",
      "description": "Detects the usage of untrusted / unescaped variables in DML queries.",
      "severity": "Medium",
      "solution": "Avoid untrusted/unescaped variables in DML query",
      "location": {
        "file": "src/classes/AppWorker.cls",
        "start_line": 59
      },
      "identifiers": [
        {
          "type": "pmd_apex_rule_id",
          "name": "PMD Apex Rule ID Security-ApexSOQLInjection",
          "value": "Security-ApexSOQLInjection",
          "url": "https://pmd.github.io/pmd-6.44.0/pmd_rules_apex_security.html#apexsoqlinjection"
        }
      ]
    },
    {
      "id": "18a84c4503cea259d868a0a5a490d596bc1a6ad95df738d62cddb9f9f7c26837",
      "name": "ApexSharingViolations",
      "description": "Detect classes declared without explicit sharing mode if DML methods are used. This forces the developer to take access restrictions into account before modifying objects.",
      "severity": "Medium",
      "solution": "Apex classes should declare a sharing model if DML or SOQL/SOSL is used",
      "location": {
        "file": "src/classes/AppWorker.cls",
        "start_line": 16
      },
      "identifiers": [
        {
          "type": "pmd_apex_rule_id",
          "name": "PMD Apex Rule ID Security-ApexSharingViolations",
          "value": "Security-ApexSharingViolations",
          "url": "https://pmd.github.io/pmd-6.44.0/pmd_rules_apex_security.html#apexsharingviolations"
        }
      ]
    },
    {
      "id": "0d7b5050f86dd5c2ae195a4e70b204ec0019bba7df65e494472cc4f9dec536d1",
      "name": "ApexSharingViolations",
      "description": "Detect classes declared without explicit sharing mode if DML methods are used. This forces the developer to take access restrictions into account before modifying objects.",
      "severity": "Medium",
      "solution": "Apex classes should declare a sharing model if DML or SOQL/SOSL is used",
      "location": {
        "file": "src/classes/AppWorker.cls",
        "start_line": 57
      },
      "identifiers": [
        {
          "type": "pmd_apex_rule_id",
          "name": "PMD Apex Rule ID Security-ApexSharingViolations",
          "value": "Security-ApexSharingViolations",
          "url": "https://pmd.github.io/pmd-6.44.0/pmd_rules_apex_security.html#apexsharingviolations"
        }
      ]
    },
    {
      "id": "a9f1b4f1b69bbafba2d5661893b23ae01269359abfd2e4929d5e1fa30d7fbd41",
      "name": "ApexSuggestUsingNamedCred",
      "description": "Detects hardcoded credentials used in requests to an endpoint. You should refrain from hardcoding credentials: They are hard to mantain by being mixed in application code, Particularly hard to update them when used from different classes, Granting a developer access to the codebase means granting knowledge of credentials, keeping a two-level access is not possible, Using different credentials for different environments is troublesome and error-prone. Instead, you should use Named Credentials and a callout endpoint.",
      "severity": "Medium",
      "solution": "Suggest named credentials for authentication",
      "location": {
        "file": "src/classes/AppWorker.cls",
        "start_line": 69
      },
      "identifiers": [
        {
          "type": "pmd_apex_rule_id",
          "name": "PMD Apex Rule ID Security-ApexSuggestUsingNamedCred",
          "value": "Security-ApexSuggestUsingNamedCred",
          "url": "https://pmd.github.io/pmd-6.44.0/pmd_rules_apex_security.html#apexsuggestusingnamedcred"
        }
      ],
      "links": [
        {
          "url": "https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_callouts_named_credentials.htm"
        }
      ]
    }
  ],
  "scan": {
    "analyzer": {
      "id": "pmd-apex",
      "name": "PMD.Apex",
      "vendor": {
        "name": "GitLab"
      },
      "version": "3.0.0"
    },
    "scanner": {
      "id": "pmd-apex",
      "name": "PMD.Apex",
      "url": "https://pmd.github.io",
      "vendor": {
        "name": "GitLab"
      },
      "version": "6.44.0"
    },
    "type": "sast",
    "status": "success",
    "start_time": "2022-02-22T22:20:22",
    "end_time": "2022-02-22T22:22:22"
  }
}
